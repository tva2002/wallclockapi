variable "server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 80
}


variable "instance_security_group_name" {
  description = "The name of the security group for the EC2 Instances"
  type        = string
  default     = "terraform-wallclock-instance"
}

variable "region" {
  description = "Region"
  type        = string
  default     = "eu-west-1"


}

variable "access_key" {
  description = "key"
  type        = string
 default     = "$AWS_ACCESS_KEY_ID"

}

variable "secret_key" {

  description = "keyS"
  type        = string
default     = "$AWS_SECRET_ACCESS_KEY"

}
