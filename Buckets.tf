#terraform {
#  backend "s3" {
#     bucket         = "wallclock"
#     key            = "./terraform.tfstate"
#     region         = "eu-central-1"
#     dynamodb_table = "wallclock_table"
#    # encrypt        = true
#
#  }
#}

terraform {
  required_providers {
    aws = {
      version = "~> 2.0"
      source = "hashicorp/aws"
    }
  }
}

terraform {
  backend "http" {
    
  }
}
