#!/usr/bin/env python
import ntplib
from time import ctime
from datetime import datetime, date, time
from flask import Flask, jsonify, make_response
from multiprocessing import Value

counter = Value('i', 0)

def GetNTPDateTime(server, counts):
    ntpDate = None
    client = ntplib.NTPClient()
    response = client.request(server, version=3)
    response.offset
    ntpDate = ctime(response.tx_time)
    datTime = datetime.strptime(ntpDate, "%a %b %d %H:%M:%S %Y").strftime("%Y-%m-%d %H:%M:%S").split(' ')
    return jsonify({'date': datTime[0], 'time': datTime[1], 'backend': "i-"+str(counts)+"deadbeaf"})



app = Flask(__name__)


@app.route('/')
def index():
    return jsonify({'hello': "world"})

@app.route('/healthz', methods=['GET'])
def get_healthz():
    return make_response(jsonify(), 200)

@app.route('/datetime', methods=['GET'])
def get_datetime():
    result = None
    with counter.get_lock():
        counter.value += 1
        out = counter.value
    try:
       result=GetNTPDateTime('0.uk.pool.ntp.org', out)
    except Exception as e:
       result=GetNTPDateTime('1.uk.pool.ntp.org', out)
    return make_response(result, 200)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
