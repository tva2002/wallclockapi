# Deploy python project to aws using GitLab CI and Terraform

## How to use

### Stages of the build
1. **Validate** - Will validate the configuration files in the working directory.
2. **Plan** - Will run terraform plan and create an execution plan.
3. **Apply** - Will run terraform apply and execute the plan. This is a manual step.
4. **Destroy** - Will destroy all resources created in the apply stage. This is a manual step as well.

### Apply
In order to execute `terraform apply` navigate to the `CI/CD` section of your project. Click on `New Pipeline`. And run a new pipeline.
Once the `validate` and `plan` stages have been completed. Click on the `apply` step and run.

### Destroy
To destroy the deployment click on the `destroy` step in the CD/CD console and run. 

## Variables
You will need to navigate to the `Settings` page of the project and under `CI/CD` you can set yuor variables.

### Suggested values for environment variables:
| Var Name               | Var Value                  | Var Type |
| :------:               |  :------:                  | :------: |
| AWS_ACCESS_KEY_ID      | Your AWS Access Key        | String   |
| AWS_SECRET_ACCESS_KEY  | Your AWS Secret Key        | String   |

## GitLab Runner

When using this project make sure you have a gitlab runner registered. You can use follow the instruction in gitlab docs to setup a new runner: [GitLab Runner Installation](https://docs.gitlab.com/runner/install/).

If you already have a runner active. You can simply add this configuration:

```
#Register the runner
sudo gitlab-runner register \
  --non-interactive \
  --url "<Your url>" \
  --registration-token "<Your project token>" \
  --executor "docker" \
  --docker-image hashicorp/terraform \
  --description "docker-runner" \
  --tag-list "" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```

