#! /bin/bash
sudo apt-get update
sudo apt-get install -y apache2
sudo apt install -y git
sudo mkdir /var/www/html/git
sudo chmod +r /var/www/html/git
sudo cd /var/www/html/git
sudo git clone https://gitlab.com/tva2002/wallclockapi.git /var/www/html/git
sudo chmod +r /var/www/html/git/wallclockapi
sudo systemctl start apache2
sudo systemctl enable apache2
echo "<h1>Deployed via Terraform</h1>" | sudo tee /var/www/html/index.html