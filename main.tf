
#provider "aws" {
#  region = "us-east-2"
#   shared_credentials_file = "/home/wat/.aws/credentials"
#   profile = "default"
#}

provider "aws" {
#  region  = "eu-central-1"
#  shared_credentials_file = "/home/gitlab-runner/.aws/credentials"
#  profile = "default"
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key

}

data "aws_subnet_ids" "default" {
    vpc_id = data.aws_vpc.default.id
}

data "aws_vpc" "default" {
default = true
}



resource "aws_security_group" "alb" {
    name = "terraform-alb-security-group"

    # Разрешить входящие HTTP
    ingress {
    from_port = 80
    to_port   = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    }
    # Разрешить все исходящие
    egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
}

# Эта группа безопасности применяется к ресурсу aws_launch_configuration
resource "aws_security_group" "instance" {
#  name = "terraform-instance-security-group"
  name = var.instance_security_group_name

  ingress {
    from_port       = var.server_port
    to_port         = var.server_port
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    }

 ingress {
    from_port = 443
    to_port   = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

    }
 egress {
    from_port = 0
    to_port   = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    }

}

#############################################################################
# ALB (Application Load Balancer)
#############################################################################

resource "aws_lb" "alb" {
    name = "terraform-alb"
    load_balancer_type = "application"
    subnets = data.aws_subnet_ids.default.ids
    security_groups = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
    load_balancer_arn = aws_lb.alb.arn
    port = 80
    protocol = "HTTP"

# Страница 404 если будут запросы, которые не соответствуют никаким правилам прослушивателя.
    default_action {
        type = "fixed-response"
        fixed_response {
        content_type = "text/plain"
        message_body = "404: страница не найдена"
        status_code = 404
        }
    }
}


# Включаем правило прослушивателя, которое отправляет запросы,
# соответствующие любому пути, в целевую группу для ASG.
resource "aws_lb_listener_rule" "asg-listener_rule" {
    listener_arn    = aws_lb_listener.http.arn
    priority        = 100

    condition {
        path_pattern {
         values = ["*"]
          }
      }
    action {
        type = "forward"
        target_group_arn = aws_lb_target_group.asg-target-group.arn
          }
}


# Создаём целевую группу aws_lb_target_group для ASG.
# Каждые 15 сек. будут отправляться HTTP запросы и если ответ 200, то все ОК, иначе
# произойдет переключение на доступный инстанс.
resource "aws_lb_target_group" "asg-target-group" {
    name = "terraform-aws-lb-target-group"
    port = var.server_port
    protocol = "HTTP"
    vpc_id = data.aws_vpc.default.id

    health_check {
        path                = "/"
        protocol            = "HTTP"
        matcher             = "200"
        interval            = 15
        timeout             = 3
        healthy_threshold   = 2
        unhealthy_threshold = 2
    }
}


#############################################################################
# ИНСТАНСЫ
#############################################################################

resource "aws_autoscaling_group" "debian-ec2" {
    launch_configuration = aws_launch_configuration.debian-ec2.name
    vpc_zone_identifier = data.aws_subnet_ids.default.ids

    target_group_arns = [aws_lb_target_group.asg-target-group.arn]
    health_check_type = "ELB"

    min_size = 2
    max_size = 5

    lifecycle {
        create_before_destroy = true
    }

    tag {
      key = "Name"
      value = "terraform-asg-debian-ec2"
      propagate_at_launch = true
     }
}

resource "aws_launch_configuration" "debian-ec2" {
  image_id        = "ami-0245697ee3e07e755"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.instance.id]
  user_data = file("./sh/wallclock.sh")

    lifecycle {
        create_before_destroy = true
    }
}


#############################################################################
# DNS ALB
############################################################################

output "alb_dns_name" {
    value = aws_lb.alb.dns_name
    description = "Доменное имя ALB"
}
